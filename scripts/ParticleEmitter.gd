extends Node2D

onready var EmitterTween = $Tween

export(Texture) var ParticleTexture
export(int) var ParticleLifetime = 1
export(Color) var InitialColor = Color(1,1,1,1)
export(Color) var FinalColor = Color(1, 1, 1, 0)

func emit():
	var s = Sprite.new()
	s.scale = Vector2(1 / scale.x, 1 / scale.y)
	var m = scale * 7
	randomize()
	s.position += Vector2((randi() % int(m.x)) - m.x / 2,
						(randi() % int(m.y)) - m.y / 2)
	
	s.set_texture(ParticleTexture)
	EmitterTween.interpolate_property(s, "position", s.position, s.position - Vector2(0, 10),
									ParticleLifetime, Tween.TRANS_QUAD,Tween.EASE_OUT)
	EmitterTween.interpolate_property(s, "self_modulate", Color(1,1,1,1), FinalColor,
								ParticleLifetime, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	add_child(s)
	EmitterTween.start()

func _on_tween_completed(object, key):
	if is_a_parent_of(object):
		remove_child(object)
		object.queue_free()