extends CanvasLayer

onready var Tweener = $Tween
onready var IntroText = $Base/IntroText

var text_pos = 0
var initial_scale
var ending_scale

const TextStrings = [
	"Break!",
	"The!",
	"Boards!!!"
]

func _ready():
	initial_scale = IntroText.rect_scale
	ending_scale = initial_scale + Vector2(1.5, 1.5)
	IntroText.set_text(TextStrings[0])
	
	start_tween()

# this blocks all input while the intro is playing
# if this isn't here then all during the intro the player can just tap away
func _unhandled_input(event):
	if get_tree().paused and text_pos < len(TextStrings):
		$Base.accept_event()

# this function just sets up the tweening stuff
func start_tween():
	Tweener.interpolate_property(IntroText, "self_modulate", Color(1,1,1,1), Color(1,1,1,0), 1.5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	Tweener.interpolate_property(IntroText, "rect_scale", initial_scale, ending_scale, 1.5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	Tweener.start()

func _on_tween_completed(object, key):
	# this is kinda arbitrary
	# the rect_scale is technically the last tween to finish in each stage
	if str(key) == ":rect_scale":
		text_pos += 1
		
		if text_pos == len(TextStrings) - 1:
			# if we display "TAP!" then we unpause
			get_tree().paused = false
		elif text_pos >= len(TextStrings):
			# if we've just displayed "TAP!" then we stop tweening
			# and hide the text and return
			Tweener.stop_all()
			IntroText.hide()
			return
		
		# if we're still displaying text then update
		# and start tweening
		IntroText.set_text(TextStrings[text_pos])
		start_tween()