extends Node

var TimeLimit = 5.0
var Points = 0
var StorePurchases = 0 setget apply_purchase

const SaveFile = "user://game.save" # the path to the save file
const SavePass = "asdf1234"         # in case we start encrypting these saves

func save_data():
	var file = File.new()
	file.open(SaveFile, File.WRITE)
	
	var data = {
		'current_points': Points,
		'purchases': StorePurchases,
		}
	
	file.store_line(to_json(data))
	file.close()

func load_data():
	var file = File.new()
	if not file.file_exists(SaveFile):
		return
	
	file.open(SaveFile, File.READ)
	var save_dict = parse_json(file.get_line())
	file.close()
	
	Points = save_dict['current_points']
	StorePurchases = save_dict['purchases']

func apply_purchase(p):
	print(p)
	pass