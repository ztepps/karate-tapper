extends Control

onready var PriceError = $PriceDialog

const ItemInfo = [
	{ 'bit': 1, 'price': 10 },
]

func _ready():
	# should write code here that parses GobalVars.StorePurchases and disables buttons
	pass

func _process_purchase(id):
	var info = ItemInfo[id]
	
	if GlobalVars.Points >= info['price']:
		GlobalVars.StorePurchases |= info['bit']
	else:
		PriceError.popup_centered()




func _on_PriceErrorButton_pressed():
	PriceError.hide()
